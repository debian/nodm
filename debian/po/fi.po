# Copyright (C) 2009
# This file is distributed under the same license as the nodm package.
#
# Esko Arajärvi <edu@iki.fi>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: nodm\n"
"Report-Msgid-Bugs-To: nodm@packages.debian.org\n"
"POT-Creation-Date: 2017-01-23 18:59+0100\n"
"PO-Revision-Date: 2010-10-18 22:13+0300\n"
"Last-Translator: Esko Arajärvi <edu@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../nodm.templates:2001
msgid "Enable nodm?"
msgstr ""

#. Type: boolean
#. Description
#: ../nodm.templates:2001
msgid ""
"Designed for embedded or kiosk systems, nodm starts an X session for a user "
"without asking for authentication. On regular machines, this has security "
"implications and is therefore disabled by default."
msgstr ""
"Koska nodm on suunniteltu sulautettuihin ja kioskijärjestelmiin, se "
"käynnistää X-istunnon ilman tunnistautumista. Tavallisilla koneilla tämä on "
"tietoturvariski ja siksi ohjelma on oletuksena poissa käytöstä."

#. Type: boolean
#. Description
#: ../nodm.templates:2001
msgid "You should enable nodm only if you need autologin on this machine."
msgstr ""
"nodm tulisi ottaa käyttöön vain, jos koneella tarvitaan automaattista "
"kirjautumista."

#. Type: string
#. Description
#: ../nodm.templates:3001
msgid "User to start a session for:"
msgstr "Käyttäjä, jolle istunto avataan:"

#. Type: string
#. Description
#: ../nodm.templates:3001
msgid ""
"Please enter the login name of the user that will automatically be logged "
"into X by nodm."
msgstr ""
"Anna käyttäjätunnus, joka kirjataan automaattisesti sisään X-istuntoon nodm:"
"n avulla."

#. Type: string
#. Description
#: ../nodm.templates:4001
msgid "Lowest numbered vt on which X may start:"
msgstr "Pienimmän virtuaalipäätteen numero, jossa X voidaan käynnistää:"

#. Type: string
#. Description
#: ../nodm.templates:4001
msgid ""
"nodm needs to find a free virtual terminal on which to start the X server."
msgstr "nodm tarvitsee vapaan virtuaalipäätteen X-palvelimen käynnistykseen."

#. Type: string
#. Description
#: ../nodm.templates:4001
msgid ""
"Since X and getty get to conflict, this parameter will specify the lowest "
"numbered virtual terminal on which to start the search."
msgstr ""
"Koska X ja getty saattavat häiritä toisiaan, tämä parametri kertoo pienimmän "
"virtuaalipäätteen numeron, josta etsintä aloitetaan."

#. Type: string
#. Description
#: ../nodm.templates:4001
msgid ""
"This value should be set to one higher than the highest numbered virtual "
"terminal on which a getty may start."
msgstr ""
"Tämän numeron tulisi olla yhtä suurempi kuin suurin sellaisen "
"virtuaalipäätteen numero, jossa getty saatetaan käynnistää."

#. Type: string
#. Description
#: ../nodm.templates:5001
msgid "Options for the X server:"
msgstr "X-palvelimen valitsimet:"

#. Type: string
#. Description
#: ../nodm.templates:5001
msgid "Please enter the options to pass to the X server when starting it."
msgstr ""

#. Type: string
#. Description
#: ../nodm.templates:5001
msgid ""
"The Xserver executable and the display name can be omitted, but should be "
"placed in front, if nodm's defaults shall be overridden."
msgstr ""

#. Type: string
#. Description
#: ../nodm.templates:5001
msgid "If no vtN option is used, nodm will perform automatic vt allocation."
msgstr ""

#. Type: string
#. Description
#: ../nodm.templates:6001
msgid "Minimum time (in seconds) for a session to be considered OK:"
msgstr "Istunnon vähimmäispituus (sekunteina):"

#. Type: string
#. Description
#: ../nodm.templates:6001
msgid ""
"If an X session will run for less than this time in seconds, nodm will wait "
"an amount of time before restarting the session. The waiting time will grow "
"until a session lasts longer than this amount."
msgstr ""
"Jos istunto on lyhyempi kuin tässä annettu vähimmäispituus, nodm odottaa "
"jonkin aikaa ennen uuden istunnon käynnistämistä. Odotusaika kasvaa kunnes "
"istunto on tätä vähimmäisaikaa pidempi."

#. Type: string
#. Description
#: ../nodm.templates:7001
#, fuzzy
#| msgid "Minimum time (in seconds) for a session to be considered OK:"
msgid "Maximum time (in seconds) to wait for X to start:"
msgstr "Istunnon vähimmäispituus (sekunteina):"

#. Type: string
#. Description
#: ../nodm.templates:7001
msgid ""
"Timeout (in seconds) to wait for X to be ready to accept connections. If X "
"is not ready before this timeout, it is killed and restarted."
msgstr ""

#. Type: string
#. Description
#: ../nodm.templates:8001
msgid "X session to use:"
msgstr "Käytettävä X-istunto:"

#. Type: string
#. Description
#: ../nodm.templates:8001
msgid "Please choose the name of the X session script to use with nodm."
msgstr ""

#. Type: select
#. Description
#: ../nodm.templates:10001
msgid "Default display manager:"
msgstr ""

#. Type: select
#. Description
#: ../nodm.templates:10001
msgid ""
"A display manager is a program that provides graphical login capabilities "
"for the X Window System."
msgstr ""

#. Type: select
#. Description
#: ../nodm.templates:10001
msgid ""
"Only one display manager can manage a given X server, but multiple display "
"manager packages are installed. Please select which display manager should "
"run by default."
msgstr ""

#. Type: select
#. Description
#: ../nodm.templates:10001
msgid ""
"Multiple display managers can run simultaneously if they are configured to "
"manage different servers; to achieve this, configure the display managers "
"accordingly, edit each of their init scripts in /etc/init.d, and disable the "
"check for a default display manager."
msgstr ""

#~ msgid "Start nodm on boot?"
#~ msgstr "Käynnistetäänkö nodm käynnistettäessä järjestelmä?"

#~ msgid ""
#~ "Please enter the options to pass to the X server when starting the "
#~ "session. These options will be used in the NODM_X_OPTIONS variable in the "
#~ "command line used by nodm to start the X session:"
#~ msgstr ""
#~ "Syötä X-palvelimelle istunnon käynnistyksen yhteydessä annettavat "
#~ "valitsimet. Nämä valitsimet annetaan muuttujassa NODM_X_OPTIONS nodm:n "
#~ "suorittamassa X-istunnon käynnistävässä komennossa."

#~ msgid "xinit program to use:"
#~ msgstr "Käytettävä xinit-ohjelma:"

#~ msgid ""
#~ "Please choose the name of the \"xinit\" program to use with nodm. This "
#~ "name will be stored in the NODM_XINIT variable in the command line used "
#~ "by nodm to start the X session:"
#~ msgstr ""
#~ "Valitse nodm:n kanssa käytettävä ”xinit”-ohjelma. Ohjelman nimi annetaan "
#~ "muuttujassa NODM_XINIT nodm:n suorittamassa X-istunnon käynnistävässä "
#~ "komennossa."

#~ msgid ""
#~ "Please choose the name of the X session script to use with nodm. This "
#~ "name will be stored in the NODM_XSESSION variable in the command line "
#~ "used by nodm to start the X session:"
#~ msgstr ""
#~ "Valitse nodm:n kanssa käytettävän X-istunnon komentosarjan nimi. Tämä "
#~ "nimi annetaan muuttujassa NODM_XSESSION nodm:n suorittamassa X-istunnon "
#~ "käynnistävässä komennossa."
